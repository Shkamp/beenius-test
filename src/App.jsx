import React, { Component } from 'react';
import './App.css';
import "@fortawesome/fontawesome-free/css/all.min.css";
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import Users from './Components/Users';
import User from './Components/User';
import Album from './Components/Album';
import Error from './Components/Error';
import Navigation from './Components/Navigation';
import {
  Breadcrumbs

} from 'react-breadcrumbs-dynamic'

import {
  BrowserRouter,
  Route,
  Switch,
} from 'react-router-dom';


class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div >
          <div >
            <Navigation />
          </div>
          <div >

            <div style={{
              padding:"50px"
            }}>
              <Breadcrumbs
                separator=" > "
              />

              <Switch>
                <Route path="/" component={Users} exact >
                </Route>
                <Route path="/users/:userId" component={User}>
                </Route>
                <Route path="/albums/:albumId" component={Album}></Route>
                <Route component={Error} />
              </Switch>

            </div>

          </div>
        </div>
      </BrowserRouter>

    );
  }
}

export default App;
