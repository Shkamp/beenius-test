import React, { Component } from 'react';
import Gallery from 'react-photo-gallery';
import Lightbox from 'react-images';
import { BreadcrumbsItem } from 'react-breadcrumbs-dynamic'

const axios = require('axios');


export default class Album extends Component {

    constructor(props) {
        super(props);
        this.state = {
            albumId: props.match.params.albumId,
            photos: [],
            currentImage: 0
        }
        this.closeLightbox = this.closeLightbox.bind(this);
        this.openLightbox = this.openLightbox.bind(this);
        this.gotoNext = this.gotoNext.bind(this);
        this.gotoPrevious = this.gotoPrevious.bind(this);
    }

    componentDidMount() {
        this.getPhotos()
    }


    getPhotos() {
        axios.get('https://jsonplaceholder.typicode.com/photos')
            .then((response) => {
                //photos = response.data;
                console.log(response);
                var downloadedPhotos = [];
                response.data.map((photo) => {
                    if (photo.albumId == this.state.albumId) {
                        var formattedPhoto = {
                            src: photo.url,
                            caption: photo.title,
                            alt: photo.title,
                            width:600,
                            height:600
                        }
                        downloadedPhotos.push(formattedPhoto);
                    }
                })
                this.setState({ photos: downloadedPhotos })
            });
    }


    openLightbox(event, obj) {
        this.setState({
            currentImage: obj.index,
            lightboxIsOpen: true,
        });
    }
    closeLightbox() {
        this.setState({
            currentImage: 0,
            lightboxIsOpen: false,
        });
    }
    gotoPrevious() {
        this.setState({
            currentImage: this.state.currentImage - 1,
        });
    }
    gotoNext() {
        this.setState({
            currentImage: this.state.currentImage + 1,
        });
    }
    render() {
        var photos = this.state.photos;

        if (!this.props.location.state) {
            window.location = "/"
        }
        return (
            <div style={{
                width: "80%",
                margin: "auto"
            }} >
                <BreadcrumbsItem to='/'>Main Page</BreadcrumbsItem>
                <BreadcrumbsItem to={'/users/' + this.props.location.state.user.id}>{this.props.location.state.user.name}</BreadcrumbsItem>
                <BreadcrumbsItem to={'/albums/' + this.state.albumId}>{this.props.location.state.album.title}</BreadcrumbsItem>
                <Gallery    
                style={{
                    width:"50%"
                }}
                photos={photos} onClick={this.openLightbox} />
                <Lightbox 
                    images={photos}
                    onClose={this.closeLightbox}
                    onClickPrev={this.gotoPrevious}
                    onClickNext={this.gotoNext}
                    currentImage={this.state.currentImage}
                    isOpen={this.state.lightboxIsOpen}
                />
            </div>
        )
    }



}