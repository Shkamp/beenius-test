import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { BreadcrumbsItem } from 'react-breadcrumbs-dynamic'

const axios = require('axios');

const styles = {
    photo: {
        width: "100%"
    }
}

export default class User extends Component {

    constructor(props) {
        super(props);
        //this.getAlbums();
        this.state = {
            userId: props.match.params.userId,
            albums: [],
            photos: []
        }
    }

    componentDidMount() {
        this.getAlbums();
        this.getPhotos();
    }

    getAlbums() {
        axios.get('https://jsonplaceholder.typicode.com/albums')
            .then((response) => {
                //albums = response.data;
                
                this.setState({ albums: response.data })
            });
    }

    getPhotos() {
        axios.get('https://jsonplaceholder.typicode.com/photos')
            .then((response) => {
                //photos = response.data;
                
                this.setState({ photos: response.data })
            });
    }

    albumPhotos(album, main) {        
        var photos = [];
        photos = this.state.photos.filter(photo => {
            return photo.albumId === album.id;
        })
        
        if (photos.length > 0) {
            
            var i = Math.floor(Math.random() * photos.length);
            if (main) {
                return photos[i].url;
            }
            return photos[i].thumbnailUrl;
        }
        return null;
    }



    render() {
        if (!this.props.location.state) {
            window.location = "/"
        }
        var albums = this.state.albums;
        var userId = this.state.userId;
        var userAlbums = albums.filter((album) => {
            return album.userId == userId;
        }
        );
        return (

            <div className="row">
                <BreadcrumbsItem to='/'>Main Page</BreadcrumbsItem>
                <BreadcrumbsItem to={'/users/' + userId}>{this.props.location.state.user.name}</BreadcrumbsItem>
                {userAlbums.map(album =>
                    <div key={album.id} className="card col-md-4 col-lg-3">
                        <Link to={{
                            pathname: "/albums/" + album.id,
                            state: {
                                user: this.props.location.state.user,
                                album: album,
                                prevPath: "/users/" + userId + "/" + album.id
                            }

                        }}>
                            <div className="row">

                                <p>{album.title}</p>
                            </div>
                            <img className="card-img-top" src={this.albumPhotos(album, true)} alt="Card image cap" />
                            <table className="row">
                                <tbody>
                                    <tr>
                                        <td><img style={styles.photo} src={this.albumPhotos(album)} alt="Card image cap" /></td>
                                        <td><img style={styles.photo} src={this.albumPhotos(album)} alt="Card image cap" /></td>
                                        <td><img style={styles.photo} src={this.albumPhotos(album)} alt="Card image cap" /></td>
                                    </tr>
                                </tbody>
                            </table>


                            <div className="card-body">


                                <h4 className="card-title">{album.name}</h4>


                                <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>

                            </div>

                        </Link>
                    </div>

                )}
            </div>

        )





    }



}