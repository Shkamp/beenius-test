import React, { Component } from 'react';
import { Link, } from 'react-router-dom';
import { BreadcrumbsItem } from 'react-breadcrumbs-dynamic';

const axios = require('axios');


export default class Users extends Component {

    constructor(props) {
        super(props);

        this.state = {
            users: [],
            albums: [],
            photos: []
        }
    }

    componentDidMount() {
        this.getUsers();
        this.getAlbums();
        this.getPhotos();
    }



    getUsers() {
        axios.get('https://jsonplaceholder.typicode.com/users')
            .then((response) => {
                //users = response.data;

                this.setState({ users: response.data })
            });
    }

    getAlbums() {
        var users = this.state;
        axios.get('https://jsonplaceholder.typicode.com/albums')
            .then((response) => {
                //albums = response.data;

                this.setState({ albums: response.data })
            });
    }

    getPhotos() {
        axios.get('https://jsonplaceholder.typicode.com/photos')
            .then((response) => {
                //photos = response.data;

                this.setState({ photos: response.data })
            });
    }



    filterAlbums(user) {

        var filteredAlbums = this.state.albums.filter(
            album => {
                return album.userId == user.id;
            }
        )

        return filteredAlbums;
    }

    randomPhoto(user) {
        var albums = this.filterAlbums(user);

        var photos = [];
        this.state.photos.map(
            photo => {
                for (let i = 0; i < albums.length; i++) {
                    if (photo.albumId == albums[i].id) {
                        photos.push(photo);
                    }
                }
            }
        );

        if (photos.length > 0) {

            var i = Math.floor(Math.random() * photos.length);
            return photos[i].url;
        }
        return null;
    }


    render() {
        if (this.state.users.length === 0 ||
            this.state.albums === 0 ||
            this.state.photos === 0) {
            return <div>
                Loading
                </div>;
        }
        var users = this.state.users;
        return (

            <div className="row">
                <BreadcrumbsItem to='/'>Main Page</BreadcrumbsItem>
                {users.map(user =>

                    <div key={user.id} className="card col-md-4 col-lg-3">

                        <Link to={{
                            pathname: "/users/" + user.id,
                            state: {
                                user: user,
                                albums: this.filterAlbums(user)
                            }
                        }}>
                            <img className="card-img-top" src={this.randomPhoto(user)} alt="Card image cap" />

                        </Link>
                        <div className="card-body">

                            <Link to={{
                                pathname: "/users/" + user.id,
                                state: {
                                    user: user,
                                    albums: this.filterAlbums(user)
                                }
                            }}>
                                <h4 className="card-title">{user.name}</h4>


                                <h5 className="card-title">{user.username}</h5></Link>
                            <h5 className="card-title"><a style={{ zIndex: "100" }} href={"mailto:" + user.email}>{user.email}</a></h5>

                            <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>


                        </div>

                    </div>


                )}


            </div>


        )



    }
}