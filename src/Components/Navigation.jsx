import React, { Component } from 'react';
import "./Navigation.css";

class Navigation extends Component {
  constructor(props) {
    super(props);
    this.toggleCollapse = this.toggleCollapse.bind(this);
    this.state = {
      collapsed: true
    }
  }


  toggleCollapse() {
    var newState = !this.state.collapsed;
    this.setState({ collapsed: newState });
  }
  getCss(element) {
    if (this.state.collapsed) {
      return element + " collapsed";
    } else {
      return element
    }
  }

  render() {
    return (
      <div>
        <div className="btn btn-default" onClick={this.toggleCollapse}>
          <i className="fas fa-bars"></i>
        </div>
        <div className={this.getCss("sidenav-bg")} onClick={this.toggleCollapse}>
          <div id="mySidenav" className={this.getCss("sidenav")}>

            <a href="javascript:void(0)" className="closebtn" onClick={this.toggleCollapse}>&times;</a>
            <a href="#">Placeholder</a>
            <a href="#">Placeholder</a>
            <a href="#">Placeholder</a>
            <a href="#">Placeholder</a>


          </div>
        </div>
      </div>
    );
  }
}

export default Navigation;